from rest_framework import serializers
from playlist.models import Song, SongPlayback


class SongSerializer(serializers.ModelSerializer):
    Lastplayed = serializers.DateTimeField(source='last_played')
    Artist = serializers.CharField(source='artist')
    Name = serializers.CharField(source='name')
    Genre = serializers.CharField(source='genre')
    Stars = serializers.IntegerField(source='stars')

    class Meta:
        model = Song
        fields = ('id', 'Lastplayed', 'Artist', 'Name', 'Genre', 'Stars')


class SongPlaybackSerializer(serializers.Serializer):
    song = SongSerializer(read_only=True)
    song_id = serializers.CharField(write_only=True)
    playbacks_count = serializers.IntegerField(read_only=True)

    def create(self, validated_data):
        return SongPlayback.objects.create(song_id=validated_data['song_id'])
