from django.db.models.aggregates import Count
from rest_framework import generics

from api.serializers import SongSerializer, SongPlaybackSerializer
from playlist.models import Song


class SongListView(generics.ListAPIView):
    queryset = Song.objects.all().prefetch_related('playbacks')
    serializer_class = SongSerializer


class SongPlaybackListView(generics.ListCreateAPIView):
    serializer_class = SongPlaybackSerializer

    def get_queryset(self):
        songs = Song.objects.all().annotate(playbacks_count=Count('playbacks'))
        return [{'song': song, 'playbacks_count': song.playbacks_count} for song in songs]
