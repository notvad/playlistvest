from django.conf.urls import url
from api.views import SongListView, SongPlaybackListView

urlpatterns = [
    url(r'^playlist/', SongListView.as_view()),
    url(r'^playbacks/', SongPlaybackListView.as_view()),
]

