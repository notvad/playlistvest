from django.contrib import admin

from playlist.models import Song


class SongAdmin(admin.ModelAdmin):
    list_display = ['artist', 'name', 'genre', 'stars']


admin.site.register(Song, SongAdmin)
