from django.db import models


class Song(models.Model):
    name = models.CharField(max_length=255)
    artist = models.CharField(max_length=255)
    stars = models.IntegerField()
    genre = models.CharField(max_length=255)

    def __unicode__(self):
        return '%s - %s' % (self.artist, self.name)

    @property
    def last_played(self):
        playbacks = self.playbacks.order_by('-created')
        return playbacks[0].created if playbacks else None


class SongPlayback(models.Model):
    song = models.ForeignKey(Song, related_name='playbacks')
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return '%s - %s' % (self.song_id, self.created)
