angular.module('Playlist.playlist', [
    'ui.router'
])

    .config(
    ['$stateProvider', '$urlRouterProvider',
        function ($stateProvider) {
            $stateProvider
                .state('home', {
                    abstract: true,
                    url: '/',
                    templateUrl: '/static/js/app/playlist.html'
                })
                .state('music', {
                    url: '',
                    views: {
                        '': {
                            templateUrl: '/static/js/app/music.html',
                            controller: ['$rootScope', '$scope', 'apiService',
                                function ($rootScope, $scope, apiService) {

                                    $scope.playlistOrder = 'Stars';
                                    $scope.nowPlaying = null;
                                    $scope.albumArts = ['joy-division.jpg', 'pink-floyd.jpg', 'velvet-undergound.jpg'];

                                    $scope.playSong = function(song) {
                                        song.Lastplayed = new Date();
                                        $scope.nowPlaying = song;
                                        var art = $scope.albumArts[Math.floor(Math.random() * $scope.albumArts.length)];
                                        $scope.nowPlaying.art = art;
                                        apiService.postSongPlayback(song.id);
                                    };

                                    $scope.changePlaylistOrder = function(playlistOrder){
                                        if ($scope.playlistOrder == playlistOrder
                                            && $scope.playlistOrder.indexOf('-') == -1){
                                            $scope.playlistOrder = '-' + $scope.playlistOrder;
                                        }
                                        else {
                                            $scope.playlistOrder = playlistOrder;
                                        }
                                    };

                                    apiService.getPlaylist().then(function(resp){
                                        $scope.playlist = resp.data;
                                    });
                                }
                            ]
                        }
                    }
                })
                .state('stats', {
                    url: '/stats',
                    views: {
                        '': {
                            templateUrl: '/static/js/app/stats.html',
                            controller: ['$rootScope', '$scope', 'apiService',
                                function ($rootScope, $scope, apiService) {

                                    function groupByField(list, field){
                                        var result = {};
                                        for (var i in list){
                                            var item = list[i][field];
                                            if (item in result){
                                                result[item] += 1
                                            }
                                            else {
                                                result[item] = 1;
                                            }
                                        }
                                        return result;
                                    }

                                    function updateChartsData(){
                                        $scope.genrePiePoints = [$scope.genres];
                                        $scope.genrePieColumns = [];

                                        for (var genre in $scope.genres){
                                            $scope.genrePieColumns.push({"id": genre, "type": "pie"});
                                        }

                                        $scope.starsPiePoints = [$scope.stars];
                                        $scope.starsPieColumns = [];

                                        for (var star in $scope.stars){
                                            $scope.starsPieColumns.push({"id": star, "type": "pie"});
                                        }
                                    }

                                    apiService.getPlaylist().then(function(resp){
                                        $scope.playlist = resp.data;
                                        $scope.genres = groupByField($scope.playlist, 'Genre');
                                        $scope.stars = groupByField($scope.playlist, 'Stars');

                                        updateChartsData();
                                    });

                                    apiService.getPlaybacks().then(function(resp){
                                        $scope.playbacks = resp.data;
                                    });

                                    $scope.genrePiePoints = [{}];
                                    $scope.genrePieColumns = [];

                                    $scope.starsPiePoints = [{}];
                                    $scope.starsPieColumns = [];
                                }
                            ]
                        }
                    }
                })
        }
    ]);