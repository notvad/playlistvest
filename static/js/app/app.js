angular.module('Playlist', [
    'Playlist.playlist',
    'ui.router',
    'gridshore.c3js.chart'
])

.run(
    [
        '$rootScope', '$state', '$stateParams',
        function ($rootScope,   $state,   $stateParams) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
    }
  ]
).service('apiService', function($http){

    this.getPlaylist = function() {
        return $http.get("/api/playlist/");
    };

    this.postSongPlayback = function(song_id) {
        return $http.post("/api/playbacks/", {song_id: song_id});
    };

    this.getPlaybacks = function() {
        return $http.get("/api/playbacks/");
    };

})
.factory('httpResponseErrorInterceptor',function($q, $injector) {
    return {
        'responseError': function(response) {
            var $http = $injector.get('$http');
            return $http(response.config);
        }
    };
})
.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider,   $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');
}])
.config(function($interpolateProvider){
    $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
})
.config(function($httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    $httpProvider.interceptors.push('httpResponseErrorInterceptor');
});
